---
layout: page
title: About
permalink: /about/
feature-img: "img/sample_feature_img_2.png"
---

This website is a WIP by Will Nozick, I am a 19 year old Computer Science Major @ San Francisco State University.

Lots of my free time I spend building android for my LG G2 (D800) and NVIDIA Shield Tablet (wx_na_do), the latter of which I maintain 2 ROM's for.
I am looking to do more than just maintain. I'm striving to successfully patch the LG G2's kernel with a kexec_hardboot patch since 4.4, for example....

WIPWIPWIP

The theme used on this Jekyll Blog is called Type Theme covered under the MIT License,
